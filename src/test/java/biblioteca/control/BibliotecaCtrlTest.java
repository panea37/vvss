package biblioteca.control;

import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import com.sun.deploy.util.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import static org.junit.Assert.*;

public class BibliotecaCtrlTest {

    BibliotecaCtrl bibliotecaCtrl;

    @Before
    public void setUp() throws Exception {
        bibliotecaCtrl = new BibliotecaCtrl(new CartiRepoMock());
    }

    @Rule
    public ExpectedException expected = ExpectedException.none();

    @After
    public void tearDown() throws Exception {

    }


    //TESTE ECP
    @Test
    public void test1_adaugaCarte() throws Exception{
        int nr;
        Carte c = new Carte();
        c.setAnAparitie("1990");
        c.setTitlu("TestTitlu");
        bibliotecaCtrl.adaugaCarte(c);
        assertEquals(7, bibliotecaCtrl.getCarti().size());
    }

    //CORECTARE
    @Test(expected = Exception.class)
    public void test2_adaugaCarte() throws Exception{
        Carte c = new Carte();
        c.setAnAparitie("numar");
        c.setTitlu("TestTitlu");
        bibliotecaCtrl.adaugaCarte(c);
        assertEquals(7, bibliotecaCtrl.getCarti().size());
    }

    @Test(expected = Exception.class)
    public void test3_adaugaCarte() throws Exception{
        Carte c = new Carte();
        c.setAnAparitie("1990");
        c.setTitlu("@TestTitlu");
        bibliotecaCtrl.adaugaCarte(c);
    }

    //TESTE BVA

    //2 INVALIDE

    @Test(expected = Exception.class)
    public void test4_adaugaCarte() throws Exception{
        Carte c = new Carte();
        c.setAnAparitie("1991");
        c.setTitlu("");
        bibliotecaCtrl.adaugaCarte(c);
    }

    //CORECTARE
    @Test(expected = Exception.class)
    public void test5_adaugaCarte() throws Exception{
        Carte c = new Carte();
        c.setAnAparitie("0");
        c.setTitlu("TestTitlu");
        bibliotecaCtrl.adaugaCarte(c);
    }

    @Test(expected = Exception.class)
    public void test5_2_adaugaCarte() throws Exception{
        Carte c = new Carte();
        c.setAnAparitie("2");
        c.setTitlu("");
        bibliotecaCtrl.adaugaCarte(c);
    }




    //2 VALIDE
    @Test
    public void test6_adaugaCarte() throws Exception{
        Carte c = new Carte();
        c.setAnAparitie("1991");
        c.setTitlu("TT");
        bibliotecaCtrl.adaugaCarte(c);
    }

    @Test
    public void test6_2_adaugaCarte() throws Exception{
        Carte c = new Carte();
        c.setAnAparitie("1991");
        c.setTitlu("T");
        bibliotecaCtrl.adaugaCarte(c);
    }


    @Test
    public void test7_adaugaCarte() throws Exception{
        Carte c = new Carte();
        c.setAnAparitie("1");
        c.setTitlu("TestTitlu");
        bibliotecaCtrl.adaugaCarte(c);
    }


    //256
    @Test(expected = Exception.class)
    public void test7_4_adaugaCarte() throws Exception{
        Carte c = new Carte();
        c.setAnAparitie("2");
        c.setTitlu("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        bibliotecaCtrl.adaugaCarte(c);
    }

    //254
    @Test
    public void test7_5_adaugaCarte() throws Exception{
        Carte c = new Carte();
        c.setAnAparitie("2");
        c.setTitlu("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        bibliotecaCtrl.adaugaCarte(c);
    }


    //255
    @Test
    public void test7_6_adaugaCarte() throws Exception{
        Carte c = new Carte();
        c.setAnAparitie("2");
        c.setTitlu("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        bibliotecaCtrl.adaugaCarte(c);
    }
}